const express = require('express');
const app = express();
const path = require('path'); 
const port = 8000;

// định nghĩa middleware để phục vụ các tệp tĩnh (css, hình ảnh, ...)
app.use(express.static('views'));

// định nghĩa route cho trang chủ
app.get('/', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/index.html' );
    res.sendFile(indexPath);
})

app.get('/shoppingcart', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const cartPath = path.join(__dirname, './views/shoppingCard.html' );
    res.sendFile(cartPath);
})

app.get('/list', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const listPath = path.join(__dirname, './views/indexListProduct.html' );
    res.sendFile(listPath);
})

// lắng nghe trên cổng 8000
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})